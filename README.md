# [Simples módulo javascript de carrinho de itens]

Simples módulo javascript de carrinho de itens com LocalStorage e Cookie (fallback).

Pessoal fiz módulo pois estou estudando mais Javascript e quero me aperfeiçoar. Quaisquer dúvidas, sugestões ou reclamações
serão bem vindas =D

## Requerimentos

1. Modernizr


## Instalação

1. Adicione o Modernizr e o arquivo carrinho.js a sua página
   

## Utilização

### cartModule.AddItem

Adiciona um item a lista, se produto já existir gera um alert informando o mesmo

### cartModule.DeleteItem

Exclui um determinado valor da lista de itens



## Features

* Utiliza localStorage
* Fallback Cookie
