/*
* Componente de carrinho de compras utilizando LocalStorage e fallback com Cookies
*/

	/*
	* Abstração 
	*/

	var Storage = (function(){
		
		var name = "itens";
		var cookieConfig = {
			end:Infinity,
			domain:window.location.host,
			path:'/',
			secure:''
		}

		return {

			check:function(){
				if(Modernizr.localstorage){

					return (localStorage.getItem(name));
				}else{
					return  (new RegExp("(?:^|;\\s*)" + escape(name).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
				}

			},
			get:function(){

				if(Modernizr.localstorage){
					console.log('aqui');
					return localStorage.getItem(name);
				}else{
					return unescape(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + escape(name).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
				}

			},
			set:function(values){

				if(Modernizr.localstorage){
					return localStorage.setItem(name,values);
				}else{
					sExpires = cookieConfig.end === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + cookieConfig.end;
					document.cookie = escape(name) + "=" + escape(values); //+ sExpires + (cookieConfig.domain ? "; domain=" + cookieConfig.domain : "") + (cookieConfig.path ? "; path=" + cookieConfig.path : "") + (cookieConfig.secure ? "; secure" : "[]");
					return true;
				}
			},
			clear:function()
			{
				if(Modernizr.localstorage){
					window.localStorage.removeItem(name);
				}else{

					if (!name || !this.check(name)) { return false; }
    				document.cookie = escape(name) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (cookieConfig.path ? "; path=" + cookieConfig.path : "");
    				return true;
				}
			}


		}

	}());



var cartComponent = (function(){

	var itens = [];
	var message = "Produto adicionado com sucesso!";
	var messageHaveItem = "Este produto já foi adicionado ao carrinho"; 
	return {
		AddItem:function(values){ //Adicionar um item ao carrinho

			if(!Storage.check())
			{
				itens.push(values);
			}else{

				if(!this.HaveItem(values)){
					itens = this.GetAll();
					itens.push(values);
				}else{
					alert(messageHaveItem);
					return false;
				}

			}
				Storage.set(JSON.stringify(itens)); 
				alert(message);

		},
		DeleteItem:function(values){ //deleta um item com o determinado valor

			itens = this.GetAll();
			if(itens.length==1)
			{
				this.Clear();
			}
			index = itens.indexOf(values);
			if(index!= -1)
			{
				itens.splice(index,1);
			}

			Storage.set(JSON.stringify(itens)); 

		},
		HaveItem:function(values){ //Verifica há um item com o determinado valor
			itens = this.GetAll();
			if(itens.indexOf(values) != -1)
			{
				return true;
			}
			return false;
		},
		GetAll:function(){ //Retorna array com itens do carrinho
			return JSON.parse(Storage.get());			
		},
		TotalItens:function(){ //Retorna total de itens no carrinho
			itens = this.GetAll();
			console.log(itens);
			return itens.length;
		},
		Clear:function(){

			Storage.clear()
		}

	}
}());

