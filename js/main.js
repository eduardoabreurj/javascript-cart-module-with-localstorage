$(document).ready(function(){

	//Carrinho

	$('.addCart').on('click',function(e){
		var id = $(this).data('id');
		
		if(!$(this).data('delete')){
			cartComponent.AddItem(id);
			$(this).data('delete','1');
			$(this).text('Remove Cart');
		}else{				
			cartComponent.DeleteItem(id);
			$(this).removeData('delete');
			$(this).text('Add Cart');
		
		}
		
		e.preventDefault();

	});

});
